# Geolocator

Esse projeto implementa uma API capaz de retornar as lojas mais próximas de um determinado ponto do globo, desde que sejam fornecidas as coordenadas de latitude e longitude. Para isso, ele faz uso de uma base de dados de lojas fake no estado de Nova Iorque, nos Estados Unidos.

### 1. Como funciona?

A base de dados foi importada para um banco espacial, que é mais adequado a armazenar e operar sobre dados geométricos do que um banco de dados relacional comum. O banco de dados espacial (para o projeto foi utilizado o PostGIS) nos entrega três funcionalidades importantes:

- representação out of the box de formas geométricas;
- um index (geralmente r-tree) capaz de responder rapidamente perguntas como: "quais os n pontos mais próximos de mim?" ou "quais são os a uma distância x de mim?";
- operações como cálculo de distância entre pontos e determinar se uma geometria contém outra.

Para realizar a higienização, transformação e importação dos dados, foi desenvolvida uma pequena aplicação em Node.js que pode ser acessada clicando [aqui](https://bitbucket.org/edearaujo/geolocator-importer/src/master/).

### 2. Como utilizar?

```
/geolocator/v1/stores?latitude=:latitude&longitude=:longitude
```

A API é bem simples e fornece apenas um endpoint. Esse endpoint recebe dois parâmetros obrigatórios, a latitude e a longitude que serão utilizadas para realizar a busca, e retorna uma lista de lojas. Cada loja tem um nome, um identificador único e a distância em relação ao ponto de busca.

Além disso, a API é paginada, então é possível selecionar qual página se deseja e qual o tamanho de cada página. Essas informações também são passadas como query parameters. Se esses parâmetros não forem passados, no entanto, serão considerados valores padrão (será retornada a primeira página com tamanho 20).

```
/geolocator/v1/stores?latitude=:latitude&longitude=:longitude&page=:page&size=:size
```

| status code | significado                                                            |
|-------------|------------------------------------------------------------------------|
| 200         | a pesquisa foi feita com sucesso: uma lista de lojas é retornada       |
| 400         | a request é inválida: campos faltando, ou valor fora do range adequado |

### 3. Como testar localmente?

- Requisitos: [NodeJS](https://nodejs.org/), [Docker](https://www.docker.com).

1) Primeiro, suba uma instância do banco de dados na sua máquina utilizando uma imagem Docker:

```
docker run --name geolocator-postgis -e POSTGRES_PASSWORD=123456 -e POSTGRES_DB=geolocator-db -e POSTGRES_USER=admin -p 5432:5432 -d mdillon/postgis
```

2) Faça um build da aplicação. Para isso, clone esse repositório na sua máquina e navegue até o diretório. Lá execute:

```
mvn clean install
```

3) Agora só falta rodar a aplicação:

```
docker run --name geolocator-app --network="host" -d edearaujo/geolocator
```

A aplicação estará disponível na porta 8080 para requests.

4) Importe a base de dados no banco utilizando o utilitário geolocator-importer. Siga as instruções [aqui](https://bitbucket.org/edearaujo/geolocator-importer/src/master/). É muito importante só importar a base de dados após subir a aplicação pelo menos uma vez, pois a aplicação contém scripts Liquibase que irão montar o modelo no banco de dados.