package edearaujo.geolocator;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

import org.json.JSONObject;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.util.LinkedMultiValueMap;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = GeolocatorApplication.class)
@AutoConfigureMockMvc
public abstract class AbstractMvcTest {

	@Autowired
	protected MockMvc mvc;

	protected <T> String toJson(T t) throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		return mapper.writeValueAsString(t);
	}

	protected <T> T toObject(JSONObject json, Class<T> _class) throws Exception {
		return new ObjectMapper().readValue(json.toString(), _class);
	}

	protected MockMvc getMockMvc() {
		return this.mvc;
	}

	protected String buildApiPath(String path) {
		return path;
	}
	
	protected ResultActions execDelete(String uri) throws Exception {
		return this.getMockMvc()
				.perform(delete(uri).contentType(MediaType.APPLICATION_JSON))
				.andDo(print());
	}
	
	protected ResultActions execPut(String uri, Object req) throws Exception {
		return this.getMockMvc()
				.perform(put(uri).contentType(MediaType.APPLICATION_JSON)
						.content(this.toJson(req)))
				.andDo(print());
	}
	
	protected ResultActions execPut(String uri, Object req, Object... uriVars) throws Exception {
		return this.getMockMvc()
				.perform(put(uri, uriVars).contentType(MediaType.APPLICATION_JSON)
						.content(this.toJson(req)))
				.andDo(print());
	}
	
	protected ResultActions execGet(String uri) throws Exception {
		return this.getMockMvc()
				.perform(get(uri).contentType(MediaType.APPLICATION_JSON))
				.andDo(print());
	}
	
	protected ResultActions execGet(String uri, LinkedMultiValueMap<String, String> params) throws Exception {
		return this.getMockMvc()
				.perform(get(uri).params(params).contentType(MediaType.APPLICATION_JSON))
				.andDo(print());
	} 
	
	protected ResultActions execPatch(String uri, Object req) throws Exception {
		return this.getMockMvc()
				.perform(patch(uri).contentType(MediaType.APPLICATION_JSON)
						.content(this.toJson(req)))
				.andDo(print());
	}
	
	protected ResultActions execPost(String uri, Object req) throws Exception {
		return this.getMockMvc()
				.perform(post(uri).contentType(MediaType.APPLICATION_JSON)
						.content(this.toJson(req)))
				.andDo(print());
	}
	
	protected ResultActions execPost(String uri, Object req, Object... uriVars) throws Exception {
		return this.getMockMvc()
				.perform(post(uri, uriVars).contentType(MediaType.APPLICATION_JSON)
						.content(this.toJson(req)))
				.andDo(print());
	}
	
	protected ResultActions execPost(String uri) throws Exception {
		return this.getMockMvc()
				.perform(post(uri).contentType(MediaType.APPLICATION_JSON))
				.andDo(print());
	}
	
}
