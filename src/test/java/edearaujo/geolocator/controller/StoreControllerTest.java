package edearaujo.geolocator.controller;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.awt.geom.Point2D;

import org.junit.Test;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.JpaSort;
import org.springframework.util.LinkedMultiValueMap;

import edearaujo.geolocator.AbstractMvcTest;
import edearaujo.geolocator.service.StoreService;

public class StoreControllerTest extends AbstractMvcTest {
	
	@MockBean
	private StoreService storeService;
	
	@Test
	public void test_search_ok() throws Exception {
		LinkedMultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("latitude", Double.toString(40.0));
		params.add("longitude", Double.toString(120.0));
		execGet("/v1/stores", params).andExpect(status().isOk());
		
		verify(storeService).findAllWithinRadiusFrom(new Point2D.Double(40.0, 120.0), PageRequest.of(0, 20, JpaSort.unsafe(Sort.Direction.ASC, "(distance)")));
	}
	
	@Test
	public void test_search_error_badRequest_missingLatitude() throws Exception {
		LinkedMultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("longitude", Double.toString(120.0));
		execGet("/v1/stores", params).andExpect(status().isBadRequest());
		
		verify(storeService, times(0)).findAllWithinRadiusFrom(any(), any());
	}
	
	@Test
	public void test_search_error_badRequest_missingLongitude() throws Exception {
		LinkedMultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("latitude", Double.toString(40.0));
		execGet("/v1/stores", params).andExpect(status().isBadRequest());

		verify(storeService, times(0)).findAllWithinRadiusFrom(any(), any());
	}
	
	@Test
	public void test_search_error_badRequest_latitudePositiveOverflow() throws Exception {
		LinkedMultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("latitude", Double.toString(90.3));
		params.add("longitude", Double.toString(120.0));
		execGet("/v1/stores", params).andExpect(status().isBadRequest());

		verify(storeService, times(0)).findAllWithinRadiusFrom(any(), any());
	}
	
	@Test
	public void test_search_error_badRequest_latitudeNegativeOverflow() throws Exception {
		LinkedMultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("latitude", Double.toString(-90.3));
		params.add("longitude", Double.toString(120.0));
		execGet("/v1/stores", params).andExpect(status().isBadRequest());

		verify(storeService, times(0)).findAllWithinRadiusFrom(any(), any());
	}
	
	@Test
	public void test_search_error_badRequest_longitudePositiveOverflow() throws Exception {
		LinkedMultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("latitude", Double.toString(40.0));
		params.add("longitude", Double.toString(180.01));
		execGet("/v1/stores", params).andExpect(status().isBadRequest());

		verify(storeService, times(0)).findAllWithinRadiusFrom(any(), any());
	}
	
	@Test
	public void test_search_error_badRequest_longitudeNegativeOverflow() throws Exception {
		LinkedMultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("latitude", Double.toString(40.0));
		params.add("longitude", Double.toString(-180.01));
		execGet("/v1/stores", params).andExpect(status().isBadRequest());

		verify(storeService, times(0)).findAllWithinRadiusFrom(any(), any());
	}
}
