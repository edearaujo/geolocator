package edearaujo.geolocator.utils;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.verify;

import java.awt.geom.Point2D;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.locationtech.proj4j.CoordinateTransform;
import org.locationtech.proj4j.ProjCoordinate;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class DatumConverterTest {

	@InjectMocks
	private DatumConverter datumConverter;
	
	@Mock
	private CoordinateTransform coordinateTransform;
	
	@Test
	public void test_transform_ok() {
		Point2D.Double point = new Point2D.Double(1.0, 2.0);
		Point2D.Double transformedPoint = new Point2D.Double(10.0, 20.0);
		
		doAnswer(i -> {
			((ProjCoordinate)  i.getArgument(1)).x = transformedPoint.getX();
			((ProjCoordinate)  i.getArgument(1)).y = transformedPoint.getY();
			return null;
		}).when(coordinateTransform).transform(any(ProjCoordinate.class), any(ProjCoordinate.class));
		
		Point2D.Double result = datumConverter.transform(point);
		verify(coordinateTransform).transform(any(ProjCoordinate.class), any(ProjCoordinate.class));
		assertEquals(transformedPoint.getX(), result.getX(), 0.0001);
		assertEquals(transformedPoint.getY(), result.getY(), 0.0001);
	}
	
}
