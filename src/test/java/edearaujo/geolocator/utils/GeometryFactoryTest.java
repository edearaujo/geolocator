package edearaujo.geolocator.utils;

import static org.junit.Assert.assertEquals;

import java.awt.geom.Point2D;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.locationtech.jts.geom.Polygon;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

@RunWith(MockitoJUnitRunner.class)
public class GeometryFactoryTest {

	@InjectMocks
	private GeometryFactory geometryFactory;
	
	private static final Integer SRID = 2222;
	
	@Before
	public void setup() {
		ReflectionTestUtils.setField(geometryFactory, "srid", SRID);
	}
	
	@Test
	public void test_createCircle_ok() {
		Point2D.Double center = new Point2D.Double(1.0, 2.0);
		double radius = 300.0;
		
		Polygon result = (Polygon) geometryFactory.createCircle(center, radius);
		assertEquals(radius, result.getLength() / (Math.PI * 2), 0.2);
		assertEquals(result.getCentroid().getX(), center.getX(), 0.0001);
		assertEquals(result.getCentroid().getY(), center.getY(), 0.0001);
	}
	
}
