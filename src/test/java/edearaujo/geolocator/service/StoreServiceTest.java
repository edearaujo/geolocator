package edearaujo.geolocator.service;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

import java.awt.geom.Point2D;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.util.GeometricShapeFactory;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.util.ReflectionTestUtils;

import edearaujo.geolocator.repository.StoreRepository;
import edearaujo.geolocator.utils.DatumConverter;
import edearaujo.geolocator.utils.GeometryFactory;

@RunWith(MockitoJUnitRunner.class)
public class StoreServiceTest {

	@InjectMocks
	private StoreService storeService;
	
	@Mock
	private StoreRepository storeRepository;
	
	@Mock
	private GeometryFactory geometryFactory;
	
	@Mock
	private DatumConverter datumConverter;
	
	private static final Double RADIUS = 3000d;
	
	@Before
	public void setup() {
		ReflectionTestUtils.setField(storeService, "radius", RADIUS);
	}
	
	@Test
	public void test_findAllWithRadius_ok() {
		Point2D.Double point = new Point2D.Double(1.0, 2.0);
		Point2D.Double transformedPoint = new Point2D.Double(10.0, 20.0);
		PageRequest pageRequest = PageRequest.of(0, 10);
		Geometry circle = new GeometricShapeFactory().createCircle();
		
		doReturn(transformedPoint).when(datumConverter).transform(point);
		doReturn(circle).when(geometryFactory).createCircle(transformedPoint, RADIUS);
		
		storeService.findAllWithinRadiusFrom(point, pageRequest);
		verify(datumConverter).transform(point);
		verify(geometryFactory).createCircle(transformedPoint, RADIUS);
		verify(storeRepository).findAllWithinRadius(circle, pageRequest);
		
	}
	
}