package edearaujo.geolocator.config;

import org.locationtech.proj4j.CRSFactory;
import org.locationtech.proj4j.CoordinateReferenceSystem;
import org.locationtech.proj4j.CoordinateTransform;
import org.locationtech.proj4j.CoordinateTransformFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Proj4jConfig {

	@Value("EPSG:${geolocator.spatial.base-srid}")
	private String baseSrid;
	
	@Value("EPSG:${geolocator.spatial.srid}")
	private String srid;
	
	@Bean
	public CoordinateTransform coordinateTransform() {
		CRSFactory crsFactory = new CRSFactory();
		CoordinateReferenceSystem srcCrs = crsFactory.createFromName(baseSrid);
		CoordinateReferenceSystem destCrs = crsFactory.createFromName(srid);
		
		return new CoordinateTransformFactory().createTransform(srcCrs, destCrs);
	}
	
}
