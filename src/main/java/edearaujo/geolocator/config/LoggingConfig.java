package edearaujo.geolocator.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.web.servlet.handler.MappedInterceptor;

import edearaujo.geolocator.logging.LoggingRegistry;
import edearaujo.geolocator.logging.RequestLogger;

@Configuration
public class LoggingConfig {

	@Bean
	@Primary
	public MappedInterceptor handlerInterceptor() {	
		LoggingRegistry loggingRegistry = LoggingRegistry.builder()
				.endpoint("search")
					.text("Lat: {}, Long: {}, Date: {}")
					.param("latitude")
					.param("longitude")
					.param("date")
				.build();
		
		return new MappedInterceptor(null, new RequestLogger(loggingRegistry));
	}
	
}
