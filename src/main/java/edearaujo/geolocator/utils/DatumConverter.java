package edearaujo.geolocator.utils;

import java.awt.geom.Point2D;

import org.locationtech.proj4j.CoordinateTransform;
import org.locationtech.proj4j.ProjCoordinate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DatumConverter {

	@Autowired
	private CoordinateTransform coordinateTransform;
	
	public Point2D.Double transform(Point2D.Double point) {
		Point2D.Double destPoint = new Point2D.Double();
		
		ProjCoordinate srcCoord = new ProjCoordinate(point.getX(), point.getY());
		ProjCoordinate destCoord = new ProjCoordinate();
		coordinateTransform.transform(srcCoord, destCoord);
		destPoint.setLocation(destCoord.x, destCoord.y);
		
		return destPoint;
	}
	
}
