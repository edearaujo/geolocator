package edearaujo.geolocator.utils;

import java.awt.geom.Point2D;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.util.GeometricShapeFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class GeometryFactory {
	
	@Value("${geolocator.spatial.srid}")
	private Integer srid;
	
	public Geometry createCircle(Point2D.Double center, Double radius) {
		GeometricShapeFactory shapeFactory = new GeometricShapeFactory();
		shapeFactory.setNumPoints(128);
		shapeFactory.setCentre(new Coordinate(center.getX(), center.getY()));
		shapeFactory.setSize(2 * radius);
		Geometry circle = shapeFactory.createCircle();
		circle.setSRID(srid);
		
		return circle;
	}
	
}
