package edearaujo.geolocator.logging;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edearaujo.geolocator.logging.LoggingRegistry.LoggingRegistryBuilder.Endpoint;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
public class LoggingRegistry {
	
	private final Map<String, Endpoint> endpoints;
	
	private LoggingRegistry(Map<String, Endpoint> endpoints) {
		this.endpoints = endpoints;
	}
	
	public static LoggingRegistryBuilder builder() {
		return new LoggingRegistryBuilder();
	}
	
	@NoArgsConstructor
	public static class LoggingRegistryBuilder {
		private Map<String, Endpoint> endpoints = new HashMap<>();
		
		public Endpoint endpoint(String name) {
			Endpoint endpoint = new Endpoint();
			endpoints.put(name, endpoint);
			
			return endpoint;
		}
		
		@Getter
		public class Endpoint {
			
			private String text;
			private List<String> params = new ArrayList<>();
			
			public Endpoint text(String text) {
				this.text = text;
				return this;
			}
			
			public Endpoint param(String param) {
				params.add(param);
				return this;
			}
			
			public Endpoint and(String name) {
				params = Collections.unmodifiableList(params);
				return LoggingRegistryBuilder.this.endpoint(name);
			}
			
			public LoggingRegistry build() {
				return LoggingRegistryBuilder.this.build();
			}
		}
		
		public LoggingRegistry build() {
			return new LoggingRegistry(Collections.unmodifiableMap(endpoints));
		}
		
	}
}
