package edearaujo.geolocator.logging;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.DispatcherType;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpMethod;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import edearaujo.geolocator.logging.LoggingRegistry.LoggingRegistryBuilder.Endpoint;
import lombok.Data;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Data
public class RequestLogger implements HandlerInterceptor {
	
	@NonNull
	private LoggingRegistry loggingRegistry;
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		
		if(DispatcherType.REQUEST.name().equals(request.getDispatcherType().name())
				&& request.getMethod().equals(HttpMethod.GET.name())
				&& handler instanceof HandlerMethod) {
		
			String methodName = ((HandlerMethod) handler).getMethod().getName();
			Endpoint endpoint = loggingRegistry.getEndpoints().get(methodName);
			
			if(endpoint != null) {
				List<String> params = new ArrayList<>();
				endpoint.getParams().stream().forEach(param -> {
					if(param.equals("date")) {
						params.add(LocalDateTime.now().toString());
						return;
					}
					
					params.add(request.getParameter(param));
				});
				log.trace(endpoint.getText(), params.toArray());		
			}
			
		}
		
		return true;
	}
	
}
