package edearaujo.geolocator.exception;

import java.time.LocalDateTime;

import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import lombok.AllArgsConstructor;
import lombok.Data;

@ControllerAdvice
public class ExceptionBarrier {

	@ExceptionHandler({ ConstraintViolationException.class })
	public ResponseEntity<ErrorResponse> handleConstraintViolationException(ConstraintViolationException ex) {		
		return errorResponse(HttpStatus.BAD_REQUEST, ex.getMessage());
	}
	
	private ResponseEntity<ErrorResponse> errorResponse(HttpStatus status, String message) {
		ErrorResponse response = new ErrorResponse(message, LocalDateTime.now());
		return ResponseEntity.status(status).body(response);
	}
	
}

@Data
@AllArgsConstructor
class ErrorResponse {
	private String message;
	private LocalDateTime timestamp;
}
