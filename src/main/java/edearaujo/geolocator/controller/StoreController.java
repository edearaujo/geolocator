package edearaujo.geolocator.controller;

import java.awt.geom.Point2D;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.JpaSort;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import edearaujo.geolocator.dto.StoreResource;
import edearaujo.geolocator.service.StoreService;

@RestController
@RequestMapping("/v1/stores")
@Validated
public class StoreController {

	@Autowired
	private StoreService storeService;
	
	@GetMapping
	public Page<StoreResource> search(
			@RequestParam(required = true) @Min(-90) @Max(90) Double latitude, 
			@RequestParam(required = true) @Min(-180) @Max(180) Double longitude,
			Pageable pageable) {		
		PageRequest pageRequest = PageRequest.of(
				pageable.getPageNumber(), pageable.getPageSize(), JpaSort.unsafe(Sort.Direction.ASC, "(distance)"));
		return storeService.findAllWithinRadiusFrom(new Point2D.Double(latitude, longitude), pageRequest);
	}
	
}
