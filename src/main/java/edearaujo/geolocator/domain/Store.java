package edearaujo.geolocator.domain;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;
import org.locationtech.jts.geom.Point;

import lombok.Data;

@Data
@Entity
public class Store {

	@Id
	@GenericGenerator(name = "uuid2", strategy = "uuid2")
	@GeneratedValue(generator = "uuid2")
	private UUID id;
	
	private String name;
	
	private String street;
	
	private String number;
	
	@Column(name = "zip_code")
	private String zipCode;
	
	private String city;
	
	private String state;
	
	private Point location;

}
