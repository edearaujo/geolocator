package edearaujo.geolocator.service;

import java.awt.geom.Point2D;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import edearaujo.geolocator.dto.StoreResource;
import edearaujo.geolocator.repository.StoreRepository;
import edearaujo.geolocator.utils.DatumConverter;
import edearaujo.geolocator.utils.GeometryFactory;

@Service
public class StoreService {

	@Autowired
	private StoreRepository storeRepository;
	
	@Autowired
	private GeometryFactory geometryFactory;
	
	@Autowired
	private DatumConverter datumConverter;
	
	@Value("${geolocator.spatial.radius}")
	private Double radius;
	
	public Page<StoreResource> findAllWithinRadiusFrom(Point2D.Double point, Pageable pageable) {
		return storeRepository.findAllWithinRadius(
				geometryFactory.createCircle(datumConverter.transform(point), radius), pageable);
	}
	
}
