package edearaujo.geolocator.repository;

import java.util.UUID;

import org.locationtech.jts.geom.Geometry;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import edearaujo.geolocator.domain.Store;
import edearaujo.geolocator.dto.StoreResource;

@Repository
public interface StoreRepository extends JpaRepository<Store, UUID> {

	@Query(
			"select s.id as id, s.name as name, distance(s.location, st_centroid(:circle)) as distance " +
			"from Store s where within(s.location, :circle) = true")
	Page<StoreResource> findAllWithinRadius(Geometry circle, Pageable pageable);
	
}
