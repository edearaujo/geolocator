package edearaujo.geolocator.dto;

public interface StoreResource {

	String getId();
	String getName();
	Double getDistance();
	
}
